// Orion Davis, 2017
// The University of Akron
// Algorithms 435 Project 2: LZW
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector> 
#include <sys/stat.h>
#include <stdio.h>

/*
  This code is derived from LZW@RosettaCode for UA CS435 
*/ 
 
// Compress a string to a list of output symbols.
// The result will be written to the output iterator
// starting at "result"; the final iterator is returned.
template <typename Iterator>
Iterator compress(const std::string &uncompressed, Iterator result) {
  // Build the dictionary.
  int dictSize = 256;
  std::map<std::string,int> dictionary;
  for (int i = 0; i < 256; i++)
    dictionary[std::string(1, i)] = i;
 
  std::string w;
  for (std::string::const_iterator it = uncompressed.begin(); it != uncompressed.end(); ++it) {
  //for (int i = 0; i < sizeof(uncompressed); i++) {
    char c = *it;
    std::string wc = w + c;
    if (dictionary.count(wc))
      w = wc;
    else {
      *result++ = dictionary[w];
      // Add wc to the dictionary. Assuming the size is 4096!!!
      if (dictionary.size()<4096)
         dictionary[wc] = dictSize++;
      w = std::string(1, c);
    }
  }
 
  // Output the code for w.
  if (!w.empty())
    *result++ = dictionary[w];
  return result;
}
 
// Decompress a list of output ks to a string.
// "begin" and "end" must form a valid range of ints
template <typename Iterator>
std::string decompress(Iterator begin, Iterator end) {
  // Build the dictionary.
  int dictSize = 256;
  std::map<int,std::string> dictionary;
  for (int i = 0; i < 256; i++)
    dictionary[i] = std::string(1, i);
 
  std::string w(1, *begin++);
  std::string result = w;
  std::string entry;
  for ( ; begin != end; begin++) {
    int k = *begin;
    if (dictionary.count(k))
      entry = dictionary[k];
    else if (k == dictSize)
      entry = w + w[0];
    else
      throw "Bad compressed k";
 
    result += entry;
 
    // Add w+entry[0] to the dictionary.
    if (dictionary.size()<4096)
      dictionary[dictSize++] = w + entry[0];
 
    w = entry;
  }
  return result;
}

// Convert a base 10 number to a binary string of cl bits
std::string int2BinaryString(int c, int cl) {
      std::string p = ""; //a binary code string with code length = cl
      while (c>0) {         
		   if (c%2==0)
            p="0"+p;
         else
            p="1"+p;
         c=c>>1;   
      }
      int zeros = cl-p.size();
      if (zeros<0) {
         p = p.substr(p.size()-cl);
      }
      else {
         for (int i=0; i<zeros; i++)  //pad 0s to left of the binary code if needed
            p = "0" + p;
      }
      return p;
}

// Convert a binary string to a base 10 integer
int binaryString2Int(std::string p) {
   int code = 0;
   if (p.size()>0) {
      if (p.at(0)=='1') 
         code = 1;
      p = p.substr(1);
      while (p.size()>0) { 
         code = code << 1; 
		   if (p.at(0)=='1')
            code++;
         p = p.substr(1);
      }
   }
   return code;
}

int main(int argc, char* argv[]) {
    // Check we are getting the right number of parameters
    if (argc != 3) {
        std::cout << "Invalid number of arguments.\n";
        std::cout << "Run the program using the command: ./lzw <mode> <filename>\n";
        return 0;
    }

    // Determine whether we are compressing or decompressing a file
    switch (*argv[1]) {

        // Compress a passed file and save it in the directory
        case 'c': {
            // Read in the file contents in preperation for
            // compression
            std::string file_contents;
            std::ifstream in_file;
            std::string in_file_name = argv[2];
            in_file.open(in_file_name.c_str(), std::ios::binary);

            // File opening was inspired by Scott Owens
            in_file.seekg(0, std::ios::end);
            file_contents.reserve(in_file.tellg());
            in_file.seekg(0, std::ios::beg);
            file_contents.assign((std::istreambuf_iterator<char>(in_file)), std::istreambuf_iterator<char>()); 

            in_file.close();

            // Compress the given contents and put the codes into a
            // vector
            std::vector<int> compressed_code;
            compress(file_contents, std::back_inserter(compressed_code));

            // Create a binary string of numbers in the vector
            std::string binary_str = "";
            for (std::vector<int>::iterator it = compressed_code.begin(); it != compressed_code.end(); ++it) {
                std::string tmp = int2BinaryString(*it, 12);
                binary_str += tmp;
            }

            // Compress the binary string and save it off
            std::string out_file_name = in_file_name + ".lzw";
            std::ofstream out_file;
            out_file.open(out_file_name.c_str(), std::ios::binary);

            // Pad the string if necessary with additional 0s to make
            // it multiple of 8
            std::string zeros = "00000000";
            if (binary_str.size() % 8 != 0) {
                binary_str += zeros.substr(0, 8-binary_str.size()%8);
            }

            int b;
            for (uint i = 0; i < binary_str.size(); i += 8) {
                b = 1;
                for (int j = 0; j < 8; j++) {
                    b = b << 1;
                    if (binary_str.at(i+j) == '1') {
                        b += 1;
                    }
                }
                char c = (char) (b & 255);
                out_file.write(&c, 1);
            }
            out_file.close();
        } break;

        // Expand the passed compressed file and write it out to be
        // compared to the original
        case 'e': {
            // Create a stream to read in the file
            std::ifstream in_file;
            std::string in_file_name = argv[2]; 
            in_file.open(in_file_name.c_str(), std::ios::binary);

            struct stat file_status;
            stat(in_file_name.c_str(), &file_status);
            long f_size = file_status.st_size;

            char c2[f_size];
            in_file.read(c2, f_size);

            // Create the binary string from the read in contents
            std::string s = "";
            std::string zeros = "00000000";
            long count = 0;
            while (count < f_size) {
                unsigned char uc = (unsigned char) c2[count];
                std::string p;
                for (int j = 0; j < 8 && uc > 0; j++) {
                    if (uc % 2 == 0) {
                        p = "0" + p;
                    }
                    else {
                        p = "1" + p;
                    }
                    uc = uc >> 1;
                }
                p = zeros.substr(0, 8-p.size()) + p;
                s += p;
                count++;
            }
            in_file.close();

            // Translate the binary back into int code to be
            // decompressed
            std::vector<int> compressed_code;

            // Iterate through the binary string taking two bytes at
            // a time and only using the first 12 bits
            std::string current_byte;
            std::string prev_byte;
            std::string two_byte_str;
            std::string bin_to_dec;
            int front = 0;

            // Read the binary values and create the original code
            // value
            for (uint bytes_read = 2; bytes_read <= s.size() / 8; bytes_read++) {
                if (s.substr(front, 12).size() < 12) {
                    continue;
                }
                std::string temp = s.substr(front, 12);
                int code = binaryString2Int(temp);
                compressed_code.push_back(code);
                front += 12;
            }

            // Run decompress function on compressed_code
            std::string decompressed = decompress(compressed_code.begin(), compressed_code.end());

            // Write the decompressed string out to a file
            std::string decompressed_file_name = argv[2];
            int pos = decompressed_file_name.find(".lzw");
            std::string out_file_name = decompressed_file_name.substr(0, pos) + "2";

            // Write the string to the file
            std::ofstream out_file;
            out_file.open(out_file_name.c_str(), std::ios::binary);
            out_file << decompressed;
            out_file.close();
        } break;
    }
  
  return 0;
}
